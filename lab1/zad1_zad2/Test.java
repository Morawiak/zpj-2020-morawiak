class Test{
    public static void main(String[] args) {
        Employee[] employees = {new Employee("Marek", 4000), new Employee("Eugeniusz", 5000)};
        System.out.println(average(employees));
        Employee biggest = (Employee)largest(employees);
        System.out.println(biggest.getName());
    }

    public static double average(Measurable[] objects){
        double avg = 0;
        int total = 0;
        for (Measurable measurable : objects) {
            avg += measurable.getMeasure();
            total++;
        }
        return avg/total;
    }

    	public static Measurable largest(Measurable[] objects){
        Measurable biggest = objects[0];
        for (Measurable measurable : objects) {
            
            if(measurable.getMeasure() > biggest.getMeasure()) biggest = measurable;
        }
        return biggest;
    }
}
