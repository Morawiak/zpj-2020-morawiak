import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class wyklad6 {
    public static void main(String[] args) {
        // Z1
        String example = "test";

        // eksperymentowanie używając kolorów ANSII
        System.out.println(String.format("Example contains %s%s",
                containsDigits(example) ? "\u001B[32mno digits" : "\u001B[31mdigits", "\u001B[0m"));

        // Z2
        try {
            Scanner in = new Scanner(new File("przyklad.txt"));
            int index = 0;
            while (in.hasNext() && index < 100) {
                String s = in.next();
                System.out.println(
                    String.format("Słowo %s %szawiera liczby i %s identyfikatorem języka Java.",
                        s,
                        containsDigits(s) ? "" : "nie ",
                        isValidIdentifier(s) ? "jest" : "nie jest"
                    )
                );
                index++;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        // Z3
        ArrayList<Character> samogloski = new ArrayList<Character>(Arrays.asList(new Character[]{'a', 'ą', 'e', 'ę', 'i', 'o', 'ó', 'u' }));
        // niestety, pracuję na windowsie, ale mogę symulować
        ArrayList<String> slowa = new ArrayList<String>();
        try {
            Scanner in = new Scanner(new File("przyklad.txt"));
            while (in.hasNext()) {
                String s = in.next();
                if (s.chars().filter(c -> samogloski.contains((char) c)).count() == 5)
                    slowa.add(s);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println("Słowa z pięcioma samogłoskami:");
        slowa.forEach(s -> System.out.println(s));

        // Z4
        String[] exampleStrings = {"ABCD", "12345679", "Sample String", "Sample String"};
        double _avg = Stream.of(exampleStrings).mapToLong(str -> str.chars().count()).sum() / exampleStrings.length;

        // Z5
        int maxLength = 0;
        for (String s : exampleStrings) {
            maxLength = Math.max(maxLength, s.length());
        }
        List<String> longestStrings = Stream.of(exampleStrings)
            .collect(Collectors.groupingBy(String::length))
            .entrySet()
            .stream()
            .max(Map.Entry.comparingByKey())
            .map(Map.Entry::getValue)
            .orElse(null);
        System.out.println("Najdłuższe słowa:");
        longestStrings.forEach(str -> System.out.println(str));

        // Z6
        /*
        Nie jest to dobry pomysl ze względu na to iż
        nie można jednoznacznie powiedzieć czy dany
        stream jest nieskończony, jest to logicznie
        niemożliwe. Mimo metody estimateSize(), tak
        jak to opisuje dokumentacja:

        ... or returns Long.MAX_VALUE if infinite,
        unknown, or too expensive to compute.

        czyli że też może zrócić jeśli to jest albo
        nieskończone, nieznane lub zbyt ciężkie aby
        obliczyć.
        */
        boolean x = isFinite(Stream.generate(() -> 0));
        System.out.println(
            String.format(
                "Ciąg jest %s",
                x ? "skończony" : "nieskończony"
            )
        );

        // Z7
        Stream<Integer> test = zip(Stream.iterate(0, n -> n + 2).limit(10), Stream.iterate(1, f -> f + 1).limit(10));
        System.out.println(
            String.format("Zipowanie dwóch streamów int'ów: %s", String.join(", ", Arrays.toString(test.toArray())))
        );

        // Z8
        // return input.reduce((a, b) -> b).get();
        // return input.reduce(new ArrayList<T>(), (a, b) -> b);
        // return input.reduce(new ArrayList<T>(), (a, b) -> b, (a, b) -> b);

        // Z9
        

        // Z11
        ArrayList<String> words = new ArrayList<String>();
        try {
            Scanner in = new Scanner(new File("przyklad.txt"));
            while (in.hasNext()) {
                words.add(in.next());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        
        long msseqbefore = System.currentTimeMillis();
        Stream.of(Arrays.toString(words.toArray()))
            .sequential()
            .collect(Collectors.groupingBy(String::length))
            .entrySet()
            .stream()
            .limit(500)
            .max(Map.Entry.comparingByKey())
            .map(Map.Entry::getValue)
            .orElse(null);
        long msseq = System.currentTimeMillis() - msseqbefore;
        long msparbefore = System.currentTimeMillis();
        Stream.of(Arrays.toString(words.toArray()))
            .parallel()
            .collect(Collectors.groupingBy(String::length))
            .entrySet()
            .stream()
            .limit(500)
            .max(Map.Entry.comparingByKey())
            .map(Map.Entry::getValue)
            .orElse(null);
        long mspar = System.currentTimeMillis() - msparbefore;

        System.out.println(
            String.format("Używając sekwencjalny stream: %sms\nUżywając równoległy stream: %sms",
            msseq,
            mspar)
        );

        // Z12
        // metodą .distinct()
    }

    public static boolean containsDigits(String input) {
        return input.codePoints().filter(c -> Character.isDigit((char) c)).count() > 0;
    }

    public static boolean isValidIdentifier(String input) {
        if (input.length() == 0)
            return false;
        if (Character.isJavaIdentifierStart(input.charAt(0))) {
            return input.substring(1, input.length()).codePoints().filter(c -> Character.isJavaIdentifierPart(c)).count() == input.length()-1;
        }
        return false;
    }

    public static <T> boolean isFinite(Stream<T> stream) {
        return stream.spliterator().estimateSize() != Long.MAX_VALUE;
    }

    public static <T> Stream<T> zip(Stream<T> pierwszy, Stream<T> drugi) {
        Iterator<T> iteratorFirst = pierwszy.iterator();
        Iterator<T> iteratorSecond = drugi.iterator();
        Stream<T> resultStream = Stream.empty();
        while (iteratorFirst.hasNext() && iteratorSecond.hasNext()){
            resultStream = Stream.concat(resultStream, Stream.of(iteratorFirst.next(), iteratorSecond.next()));
        }
        return resultStream;
    }

    public static <T> ArrayList<T> reduce1(Stream<ArrayList<T>> input) {
        return input.reduce(new ArrayList<T>(), (a, b) -> b, (a, b) -> b);
    }
}