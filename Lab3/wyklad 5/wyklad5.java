import java.io.FileInputStream;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

class wyklad5 {

    public static Stream<Long> LCG(
        long seed,
        long a,
        long c,
        long m
    ) {
        return Stream.iterate(seed, n -> {
            return (a * n + c) % m;
        });
    }
    
    static int registeredTimes = 0;
    public static void main(String[] args) {

        String src = "";
        try {
            FileInputStream fis = new FileInputStream("zemsta.txt");
            src = new String(fis.readAllBytes());
            fis.close();
        } catch (Exception e) {

        }
        List<String> words = Arrays.asList(src.split("\\s+"));

        // Z1

        words.stream()
            .limit(5)
            .filter((w) -> {
                registeredTimes++;
                return w.length() > 12;
            });

        // Z2
        long before1 = System.currentTimeMillis();
        words.stream().
            filter(w -> w.length() > 12).count();
        System.out.println(String.format("Zwykły stream zajął %s ms", System.currentTimeMillis() - before1));

        long before2 = System.currentTimeMillis();
        words.parallelStream().
            filter(w -> w.length() > 12).count();
        System.out.println(String.format("Parellel stream zajął %s ms", System.currentTimeMillis() - before2));

        // Z3
        int[] wartosci = { 1, 4, 9, 16 };
        
        Stream<int[]> s = Stream.of(wartosci);

        // aby uzyskać strumień wartości typu int
        IntStream proper = IntStream.of(wartosci);

        // Z4
        Stream<Long> LCG_Example = LCG(1, 25214903917L, 11, 248);
        LCG_Example.limit(5).forEach(x -> {
            System.out.println(String.format("LCG Przykład: %s", x.toString()));
        });

        // Z5
        characters("12345").forEach(n -> System.out.println(String.format("Characters: %s", n)));
    }

    public static Stream<Integer> characters(String s)
    {
        // return s.chars().mapToObj(c -> (char) c);

        return Stream.iterate(0, i -> i < s.length()-1, i -> i+1);
    }
}