import java.io.*;
import java.util.*;

class wyklad4 {

    static class Pracownik {
        public double wynagrodzenie = 0;
        public String nazwisko = "";
        Pracownik(String nazwisko) {
            this.wynagrodzenie = Math.random()*5000;
            this.nazwisko = nazwisko;
        }
        Pracownik(double wynagrodzenie, String nazwisko) {
            this.wynagrodzenie = wynagrodzenie;
            this.nazwisko = nazwisko;
        }
    }

    public static void main(String[] args) {
        File[] files = new File("./").listFiles();
        SortFiles(files);

        for (File file : files) {
            System.out.println(String.format("[%s]\t%s", file.isDirectory() ? "D" : "F", file.getName()));
        }

        Encapsulate(
            () -> { System.out.println("Pierwszy Runnable"); },
            () -> { System.out.println("Drugi Runnable"); },
            () -> { System.out.println("Trzeci Runnable"); }
        ).run();

        Comparator<Pracownik> compareByWszystko = Comparator
            .comparing((Pracownik p) -> { return p.wynagrodzenie; })
            .thenComparing(Comparator.comparing((Pracownik p) -> { return p.nazwisko; }));

        Pracownik[] pracownicy = new Pracownik[]{
            new Pracownik(2000, "Gorzelak"),
            new Pracownik(2000, "Karczmarczyk"),
            new Pracownik("Barczynski"),
            new Pracownik("Wernyhora")
        };
        Arrays.sort(pracownicy, compareByWszystko);

        for (Pracownik p : pracownicy) {
            System.out.println(String.format("Pracownik\t%s\tzarabia\t%s", p.nazwisko, p.wynagrodzenie));
        }

        System.out.println(String.format(
            "Implementacja RandomSequence.next() -> %s",
            new RandomSequence(1, 10).next()
        ));
    }

    private static Random generator = new Random();
    static class RandomSequence implements IntSequence {
        int low = 0;
        int high = 0;

        RandomSequence() {};
        RandomSequence(int low, int high) {
            this.low = low;
            this.high = high;
        }

        public int next() {
            return low + generator.nextInt(high - low + 1);
        }
        public boolean hasNext() { return true; }
    }

    public static void SortFiles(File[] files) {
        Arrays.sort(files, (File a, File b) -> {
            /*
            if (a.isDirectory() && !b.isDirectory())
            {
                return -1;
            } else if (!a.isDirectory() && b.isDirectory()) {
                return 1;
            }
            return a.compareTo(b);
            */
            return a.isDirectory() && !b.isDirectory() ? -1 : (
                (!a.isDirectory() && b.isDirectory()) ? 1 : a.compareTo(b)
            );
        });
    }

    public static Runnable Encapsulate(Runnable... runnables) {
        return () -> {
            for (Runnable runnable : runnables) {
                runnable.run();
            }
        };
    }
    
}