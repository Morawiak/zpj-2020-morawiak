import java.io.*;
import java.util.*;

class wyklad3 {
    public static void main(String[] args) {
        Greeter greet1 = new Greeter(3, "Arthur");
        Greeter greet2 = new Greeter(3, "Ryszard");

        runTogether(greet1, greet2);

        try {
            ListChildDirectories("../../").forEach((f) -> {
                System.out.println(String.format("Wykryto folder %s", f.toPath().toString()));
            });
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try {
            ListFilesWithExtension("./", ".java").forEach((f) -> {
                System.out.println(String.format("Znaleziono plik %s", f.toPath().toString()));
            });
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void runTogether(Runnable... tasks) {
        for (Runnable runnable : tasks) {
            (new Thread(runnable)).start();
        }
    }

    public static void runInOrder(Runnable... tasks) {
        for (Runnable runnable : tasks) {
            runnable.run();
        }
    }

    private static ArrayList<File> _recursiveHelper(File[] files) {
        ArrayList<File> result = new ArrayList<File>(Arrays.asList(files));
        for (File file : files) {
            result.addAll(_recursiveHelper(file.listFiles(
                new FileFilter() {
                        @Override
                        public boolean accept(File d) {
                            return d.isDirectory();
                        }
                }
            )));
        }
        return result;
    }

    public static ArrayList<File> ListChildDirectories(String path)
        throws FileNotFoundException
    {
        File target = new File(path);
        if (!target.exists() || !target.isDirectory()){
            throw new FileNotFoundException(String.format("Nie znaleziono folderu w %s", path));
        }
        return _recursiveHelper(target.listFiles((File d) -> { return d.isDirectory(); }));
    }



    public static ArrayList<File> ListFilesWithExtension(String path, String extension)
        throws FileNotFoundException
    {
        File target = new File(path);
        if (!target.exists() || !target.isDirectory()){
            throw new FileNotFoundException(String.format("Nie znaleziono folderu w %s", path));
        }
        String[] fileNames = target.list(
            (File dir, String name) -> {
                return name.toLowerCase().endsWith(extension.toLowerCase());
            }
        );
        ArrayList<File> files = new ArrayList<File>();
        for (String fileName : fileNames) {
            files.add(new File(String.format("%s\\%s", path, fileName)));
        }
        return files;
    }
}