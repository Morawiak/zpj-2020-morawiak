class Greeter implements Runnable {
    int n = 0;
    String target = "";

    public Greeter(int n, String target) {
        this.n = n;
        this.target = target;
    }

    @Override
    public void run() {
        for (int i = 0; i < this.n; i++) {
            System.out.println(String.format("Witaj, %s!", target));
        }
    }
    
}