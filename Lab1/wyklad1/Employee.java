public class Employee implements Measurable {
    private String imie = "";
    private String nazwisko = "";
    private double pensja = 0;
    
    public Employee() {}
    public Employee(double pensja) {
        this.pensja = pensja;
    }
    public Employee(String imie, String nazwisko, double pensja) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.pensja = pensja;
    }

    @Override
    public double getMeasure() {
        return this.pensja;
    }

    public String getImie() {
        return this.imie;
    }

    public String getNazwisko() {
        return this.nazwisko;
    }
}