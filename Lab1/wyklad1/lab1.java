import java.util.*;
import javax.imageio.stream.ImageOutputStream;

class lab1 {
    public static void main(String[] args) {
        Employee[] employees = new Employee[]{
            new Employee("Jan", "Kowalski", Math.random()*5000),
            new Employee("Mateusz", "Lisinski", Math.random()*5000),
            new Employee("Joanna", "Radzinska", Math.random()*5000),
            new Employee("Magdalena", "Gorzelak", Math.random()*5000)
        };

        System.out.println("Typ nadrzędny dla typu String jest " + String.class.getSuperclass().getName());
        System.out.println("Typ nadrzędny dla typu Scanner jest " + Scanner.class.getSuperclass().getName());

        // nie wiem czemu, jednak nie ma superklasy typu ImageOutputStream
        // ale pewnie jest i tak traktowany jako Object
        System.out.println("Typ nadrzędny dla typu ImageOutputStream jest " + ImageOutputStream.class.getSuperclass());

        System.out.println("Średnia pensja to: " + average(employees));
        Employee topPaid = (Employee)largest(employees);
        System.out.println("Najwyższą pensję ma " + topPaid.getImie() + " " + topPaid.getNazwisko() + ".");

        System.out.print("Sekwencja liczby 1, 2, 3: ");
        System.out.println(IntSequence.of(1, 2, 3));
    }

    public static double average(Measurable[] objects) {
        double avg = 0;
        for (Measurable measurable : objects) {
            avg += measurable.getMeasure();
        }
        return avg / objects.length;
    }

    public static Measurable largest(Measurable[] objects) {
        if (objects.length == 0)
            return null;
        Measurable largest = objects[0];
        for (Measurable measurable : objects) {
            largest = (largest.getMeasure() < measurable.getMeasure()) ? measurable : largest;
        }
        return largest;
    }
}