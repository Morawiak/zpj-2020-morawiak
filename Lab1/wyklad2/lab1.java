

import java.util.*;

class lab1  {
    public static void main(String[] args) {
        ArrayList<String> example = new ArrayList<String>(Arrays.asList(
            "D",
            "A",
            "C",
            "B",
            "E"
        ));
        luckySort(example, (a, b) -> a.compareTo(b));
    }

    @SuppressWarnings("unchecked")
    static void luckySort(ArrayList<String> strings, Comparator<String> comp) {
        System.out.println("Trying to get lucky at finding the correct combination!");
        ArrayList<String> correctOrder = ((ArrayList<String>)strings.clone());
        correctOrder.sort(comp);
        int permutations = 0;
        boolean correctlySorted = true;

        for (int i = 0; i < strings.size(); i++) {
            if (strings.get(i).compareTo(correctOrder.get(i)) != 0) {
                correctlySorted = false;
                break;
            }
        }

        while (!correctlySorted) {
            Collections.shuffle(strings);
            System.out.print("Trying " + (String.join(", ", strings)));
            permutations++;

            correctlySorted = true;
            for (int i = 0; i < strings.size(); i++) {
                if (strings.get(i).compareTo(correctOrder.get(i)) != 0) {
                    correctlySorted = false;
                    break;
                }
            }
            System.out.print(" --> " + (correctlySorted ? "Found it!\n" : "Fail\r"));
        }
        if (permutations > 0) {
            System.out.println("Had to shuffle " + permutations + " times before achieving correct order.");
        } else {
            System.out.println("The strings ArrayList was already correctly sorted in requested order.");
        }
    }
}