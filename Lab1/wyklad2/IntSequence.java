public final class IntSequence {
    private IntSequence() {}

    public static Runnable constant(int i) {
        return () -> {
            while (true)
                System.out.println(i);
        };
    }
}